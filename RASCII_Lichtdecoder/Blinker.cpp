/*
Copyright (c) 2018 andy      <don_funk@bluewin.ch>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created by Andreas Zogg, September 26, 2018.

Blinker.cpp - Version 1.0.1

13.11.2018 Include StandardCplusPlus Library enfernt
*/

#include "Blinker.h"

using namespace std;

// #define BLINKER_DEBUG_MSG
#define RandomSeed_Pin	7

//=======================================================
//einfacher Ausgang
//Mode 40
//Type n/a
//=======================================================
Ausgang::Ausgang(uint8_t BaseChannel_):
	accessories(BaseChannel_),
	m_Active(false),
	Output(BaseChannel_){
}
Ausgang::~Ausgang() {
};

void Ausgang::notifyCommand(uint8_t Port, uint8_t Value) {

	if (Port == m_BaseChannel) {
		// Einschalten
		if (Value == 1) {
			on();
		};

		// Ausschalten
		if (Value == 0) {
			off();
		};
	}

};

void Ausgang::on() {
	m_Active = true;
	Output.on();
};

void Ausgang::off() {
	m_Active = false;
	Output.off();
};

bool Ausgang::isOn() const {
	return m_Active;
};


//=======================================================
//Blinker mit 1 Lampen
//Mode 50
//Type n/a
//=======================================================
Blinker::Blinker(uint8_t BaseChannel_, uint16_t timeOff_, uint16_t timeOn_):
	accessories(BaseChannel_),
	m_active(false),
	m_State(false),
	m_timeOff(timeOff_),
	m_timeOn(timeOn_),
	m_startTime(millis()),
	m_Mode(50),
	m_LED1(BaseChannel_, PWM_Set_On, 0, 0) {

};

Blinker::Blinker(uint8_t BaseChannel_, uint16_t timeOff_, uint16_t timeOn_,  uint8_t Mode_):
	accessories(BaseChannel_),
	m_active(false),
	m_State(false),
	m_timeOff(timeOff_),
	m_timeOn(timeOn_),
	m_startTime(millis()),
	m_Mode(Mode_),
	m_LED1(BaseChannel_, PWM_Set_On, 40, 100) {

};

Blinker::Blinker(uint8_t BaseChannel_, uint16_t timeOff_, uint16_t timeOn_, uint8_t fadeUpTime_, uint8_t fadeDownTime_,  uint8_t Mode_):
	accessories(BaseChannel_),
	m_active(false),
	m_State(false),
	m_timeOff(timeOff_),
	m_timeOn(timeOn_),
	m_startTime(millis()),
	m_Mode(Mode_),
	m_LED1(BaseChannel_, PWM_Set_On, fadeUpTime_, fadeDownTime_) {
};

Blinker::~Blinker(){

}

void Blinker::notifyCommand(uint8_t Port, uint8_t Value) {
#ifdef BLINKER_DEBUG_MSG
	Serial.println(F("Blinker::notifyCommand"));
	Serial.print(F("Port:	 ")); Serial.println(Port);
	Serial.print(F("Channel: ")); Serial.println(m_BaseChannel);
	Serial.print(F("Value:   ")); Serial.println(Value); 
#endif

	if (Port == m_BaseChannel) {
		// Einschalten
		if (Value == 1) {
			m_active = true;
			m_State = true;
			m_currentInterval = 0;
			m_LED1.on();
		};

		// Ausschalten
		if (Value == 0) {
			m_active = false;
			m_LED1.off();
		};
	}
#ifdef BLINKER_DEBUG_MSG
	Serial.print(F("m_active: ")); Serial.println(m_active);
	Serial.print(F("m_State: ")); Serial.println(m_State);
#endif
};

void Blinker::process() {

	m_LED1.process();

	// do nothing if not active
	if (!m_active)
		return;

	// if time to do something, do it
	if (millis() - m_startTime >= m_currentInterval){
		m_startTime = millis();
		if (m_State) {
			m_LED1.on();
			m_currentInterval = m_timeOn;
		}
		else {
			m_LED1.off();
			m_currentInterval = m_timeOff;
		}
		m_State = !m_State;
	} // end of if
}

void Blinker::on() {
	m_active = true;
	m_startTime = millis();
	m_currentInterval = m_timeOn;
};

void Blinker::off() {	
	m_active = false;
	m_LED1.off();
};

bool Blinker::isOn() const {
	return m_active;
};

//=======================================================
//Wechselblinker
//Mode 51
//Type n/a
//=======================================================
Wechselblinker::Wechselblinker(uint8_t BaseChannel_, uint16_t timeOff_, uint16_t timeOn_) :
	Blinker(BaseChannel_, timeOff_, timeOn_, 51),
	m_LED2(BaseChannel_, PWM_Set_On, 0, 0) {
};

Wechselblinker::Wechselblinker(uint8_t BaseChannel_, uint16_t timeOff_, uint16_t timeOn_, uint8_t fadeUpTime_, uint8_t fadeDownTime_) :
	Blinker(BaseChannel_, timeOff_, timeOn_, fadeUpTime_, fadeDownTime_, 51),
	m_LED2(BaseChannel_ + 1, PWM_Set_On, fadeUpTime_, fadeDownTime_) {
}
Wechselblinker::~Wechselblinker() {
};

void Wechselblinker::notifyCommand(uint8_t Port, uint8_t Value) {
#ifdef BLINKER_DEBUG_MSG
	Serial.println(F("Wechselblinker::notifyCommand"));
	Serial.print(F("Port:	 ")); Serial.println(Port);
	Serial.print(F("Channel: ")); Serial.println(m_BaseChannel);
	Serial.print(F("Value:   ")); Serial.println(Value);
#endif

	if (Port == m_BaseChannel) {
		
		// Einschalten
		if (Value == 1) {
			m_active = true;
			m_State = true;
			m_startTime = 0;
			m_LED1.on();
			m_LED1.off();
			m_currentInterval = m_timeOff;
		};

		// Ausschalten
		if (Value == 0) {
			m_active = false;
			m_LED1.off();
			m_LED2.off();
		};
	}
#ifdef BLINKER_DEBUG_MSG
	Serial.print(F("m_active: ")); Serial.println(m_active);
	Serial.print(F("m_State: ")); Serial.println(m_State);
#endif
}

void Wechselblinker::process() {
	m_LED1.process();
	m_LED2.process();

	// do nothing if not active
	if (!m_active)
		return;
	
	// if time to do something, do it
	if (millis() - m_startTime >= m_currentInterval) {
		m_startTime = millis();
		if (m_State) {
			m_LED1.on();
			m_LED2.off();
			m_currentInterval = m_timeOn;
		} else {
			m_LED1.off();
			m_LED2.on();
			m_currentInterval = m_timeOff;
		}
		m_State = !m_State;
	} // end of if

}

void Wechselblinker::off() {
	m_active = false;
	m_LED1.off();
	m_LED2.off();
}

//=======================================================
//Lauflicht
//Mode 
//Type 
//=======================================================
Lauflicht::Lauflicht(uint8_t BaseChannel_,  uint8_t Anzahl_, uint16_t timeOff_, uint16_t timeOn_,  uint8_t Mode_):
	accessories(BaseChannel_),
	m_Anzahl(Anzahl_),
	m_Mode(Mode_),
	m_active(false),
	m_State(0),
	m_timeOn(timeOn_),
	m_timeOff(timeOff_),
	m_startTime(millis()) {

#ifdef BLINKER_DEBUG_MSG
	Serial.println(F("Lauflicht::Lauflicht"));
	Serial.print(F("BaseChannel: ")); Serial.println(BaseChannel_);
	Serial.print(F("Channel:     ")); Serial.println(m_BaseChannel);
	Serial.print(F("Anzahl:      ")); Serial.println(Anzahl_);
#endif

	// Anzahl LED's erstellen
	for (int i_ = 0; i_ < Anzahl_; i_++) {
		m_LED.push_back(new LEDFader(BaseChannel_ + i_));
	};

};

Lauflicht::Lauflicht(uint8_t BaseChannel_,  uint8_t Anzahl_, uint16_t timeOff_, uint16_t timeOn_, uint8_t fadeUpTime_, uint8_t fadeDownTime_,  uint8_t Mode_) :
	accessories(BaseChannel_),
	m_Anzahl(Anzahl_),
	m_Mode(Mode_),
	m_active(false),
	m_State(0),
	m_timeOn(timeOn_),
	m_timeOff(timeOff_),
	m_startTime(millis()) {

#ifdef BLINKER_DEBUG_MSG
	Serial.println(F("Lauflicht::Lauflicht"));
	Serial.print(F("BaseChannel: ")); Serial.println(BaseChannel_);
	Serial.print(F("Channel:     ")); Serial.println(m_BaseChannel);
	Serial.print(F("Anzahl:      ")); Serial.println(Anzahl_);
#endif

	// Anzahl LED's erstellen
	for (int i_ = 0; i_ < Anzahl_; i_++) {
		m_LED.push_back(new LEDFader(BaseChannel_ + i_, PWM_Set_On, fadeUpTime_, fadeDownTime_));
	};

	for (vector<LEDFader*>::const_iterator led_ = m_LED.begin(); led_ != m_LED.end(); led_++) {
		(*led_)->off();
	};

};

Lauflicht::~Lauflicht() {

	for (int i = 0; i < m_LED.size(); i++) {
		delete m_LED[i];
	};
	
	// Gr�sse von Decoder auf 0 setzen
	m_LED.clear();

	// Speicher von decoder freigeben
	vector<LEDFader*>().swap(m_LED);
};

void Lauflicht::notifyCommand(uint8_t Port, uint8_t Value) {
#ifdef BLINKER_DEBUG_MSG
	Serial.println(F("Lauflicht::notifyCommand"));
	Serial.print(F("Port:	 ")); Serial.println(Port);
	Serial.print(F("Channel: ")); Serial.println(m_BaseChannel);
	Serial.print(F("Value:   ")); Serial.println(Value);
#endif

	if (Port == m_BaseChannel) {
		// Einschalten
		if (Value == 1) {
			m_active = true;
			m_currentInterval = m_timeOn;
			m_startTime = 0;
			m_State = false;
			m_NextStep = 0;
			m_LastStep = 0;
		};

		// Ausschalten
		if (Value == 0) {
			m_active = false;
			for (vector<LEDFader*>::const_iterator it = m_LED.begin(); it != m_LED.end(); it++) {
				(*it)->off();
			};
		};
	}
#ifdef BLINKER_DEBUG_MSG
	Serial.print(F("m_active: ")); Serial.println(m_active);
	Serial.print(F("m_State: ")); Serial.println(m_State);
#endif
};

void Lauflicht::process() {

	for (vector<LEDFader*>::const_iterator led_ = m_LED.begin(); led_ != m_LED.end(); led_++) {
		(*led_)->process();
	};

	// do nothing if not active
	if (!m_active)
		return;

	// if time to do something, do it
	if (millis() - m_startTime >= m_currentInterval) {
		m_startTime = millis();
		uint8_t t_ = 0;

		switch (m_Mode) {
		case 52:
			// Kette durchgehen und notwendige LED einschalten, die anderen aussschalten
			if (m_LastStep != m_NextStep) {
				m_LED[m_LastStep]->off();
				m_LastStep = m_NextStep;
			};

			m_LED[m_NextStep]->on();
			m_NextStep++;

			if (m_NextStep >= m_Anzahl)
				m_NextStep = 0;

			break;
		case 53:
			// Alle Lampen werden nacheinander eingschaltet, am Schluss werden alle ausgeschaltet

			// Ende der Kette ist noch nicht erreicht
			if (m_NextStep != 254) {

				if (m_NextStep < m_Anzahl)
					m_LED[m_NextStep]->on();
				m_NextStep++;

				if (m_NextStep > m_Anzahl)
					m_NextStep = 254;
			}
			// Ende der Kette ist erreicht
			else {
				for (vector<LEDFader*>::const_iterator led_ = m_LED.begin(); led_ != m_LED.end(); led_++) {
					(*led_)->off();
				};

				// Pr�fen ob alle Lampen dunkel sind
				bool b_ = true;
				for (vector<LEDFader*>::const_iterator led_ = m_LED.begin(); led_ != m_LED.end(); led_++) {
					if (!(*led_)->isDark())
						b_ = false;
				};

				// Wenn alle dunkel sind  dann n�chster Step
				if (b_)
					m_NextStep = 0;
			};
			break;

		case 54:
			// Lampe f�r Lampe wird eingeschaltet. Sind alle eingeschaltet, wird mit der ersten Lampe begonnen zu l�schen.
			if (m_NextStep < m_Anzahl)
			{
				m_LED[m_NextStep]->on();			
				m_NextStep++;
				
			}
			else if (m_LastStep < m_Anzahl) {
				m_LED[m_LastStep]->off();
				m_LastStep++;
			}
			else {
				m_NextStep = 0;
				m_LastStep = 0;
			};

			break;

		case 55:
			break;

		};

		// Set on or off Intervall
		if (m_State) {
			m_currentInterval = m_timeOn;
		}
		else { 
			m_currentInterval = m_timeOff;
		};
		m_State = !m_State;

	}; // end of if
};

//=======================================================
//Hausbeleuchtung
//Mode 60
//Type n/a
// http://forum.arduino.cc/index.php?topic=159117.15
//=======================================================
Hausbeleuchtung::Hausbeleuchtung(uint8_t BaseChannel_,  uint8_t Anzahl_, uint32_t minRandomTime_, uint32_t maxRandomTime_) :
	accessories(BaseChannel_),
	m_Anzahl(Anzahl_),
	m_maxRandomTime(maxRandomTime_),
	m_minRandomTime(minRandomTime_),
	m_startTime(millis() / 1000),
	m_currentInterval(1),
	m_active(false) {

	// Anzahl LED's erstellen
	for (int i_ = 0; i_ < Anzahl_; i_++) {
		m_LED.push_back(new LED(BaseChannel_ + i_));
	};

	// Default f�r m_minRandomTime setzen wenn 0
	if (!m_minRandomTime)
		m_minRandomTime = 5;

	// Default f�r m_maxRandomTime setzen wenn 0
	if (!m_maxRandomTime)
		m_maxRandomTime = 20;

	// Alle Ausg�nge ausschalten
	for (vector<LED*>::const_iterator led_ = m_LED.begin(); led_ != m_LED.end(); led_++) {
		(*led_)->off();
	};
};

Hausbeleuchtung::~Hausbeleuchtung() {
	for (int i = 0; i < m_LED.size(); i++) {
		delete m_LED[i];
	};

	// Gr�sse von Decoder auf 0 setzen
	m_LED.clear();

	// Speicher von decoder freigeben
	vector<LED*>().swap(m_LED);
};

void Hausbeleuchtung::notifyCommand(uint8_t Port, uint8_t Value) {
#ifdef BLINKER_DEBUG_MSG
	Serial.println(F("Hausbeleuchtung::notifyCommand"));
	Serial.print(F("Port:	 ")); Serial.println(Port);
	Serial.print(F("Channel: ")); Serial.println(m_BaseChannel);
	Serial.print(F("Value:   ")); Serial.println(Value);
#endif

	if (Port == m_BaseChannel) {
		// Einschalten
		if (Value == 1) {
			m_active = true;
			randomSeed(analogRead(RandomSeed_Pin));
			m_currentInterval = random(m_minRandomTime, m_maxRandomTime);
			m_startTime = millis() / 1000;
		};

		// Ausschalten
		if (Value == 0) {
			m_active = false;
		};

	}
#ifdef BLINKER_DEBUG_MSG
	Serial.print(F("m_active: ")); Serial.println(m_active);

#endif
};

void Hausbeleuchtung::process() {

	uint32_t now_ = millis() / 1000UL;

	for (vector<LED*>::const_iterator it = m_LED.begin(); it != m_LED.end(); it++) {
		(*it)->process();
	};

	// if time to do something, do it
	if (now_ - m_startTime >= m_currentInterval) {
		uint8_t t1_ = 0;
		uint8_t	t2_ = 0;

		// Bestimmen welcher Ausgang geschaltet werden soll
		randomSeed(analogRead(RandomSeed_Pin));
		t2_ = random(m_Anzahl);

		// Bestimmen wann das n�chstemal etwas gemacht werden soll
		m_currentInterval = random(m_minRandomTime, m_maxRandomTime);
		m_startTime = now_;

		for (vector<LED*>::const_iterator it = m_LED.begin(); it != m_LED.end(); it++) {
			bool on_ = (*it)->isOn();
			// if (((*it)->isOn() == false) && (m_active) && (t1_ == t2_)) {
			if (!on_ && m_active && (t1_ == t2_)) {
				(*it)->on();
				break;
			};

			if ((on_) && (t1_ == t2_)) {
				(*it)->off();
				break;
			};

			t1_++;
		};
	};

};

//=======================================================
//Fernseher
//Mode 80
//Type n/a
// http://forum.arduino.cc/index.php?topic=159117.15
//=======================================================
Fernseher::Fernseher(uint8_t BaseChannel_) :
	Blinker(BaseChannel_, 0, 0, 0, 0, 80) {
}
Fernseher::~Fernseher() {
};

void Fernseher::process() {

	randomSeed(analogRead(RandomSeed_Pin));

	m_LED1.process();
	
	// do nothing if not active
	if (!m_active)
		return;

	// if time to do something, do it
	if (millis() - m_startTime >= m_currentInterval) {
		m_startTime = millis();
		m_LED1.SetBrightness(random(256), true);
		m_currentInterval = random(200);
	} // end of if
};

void Fernseher::on() {
	randomSeed(analogRead(RandomSeed_Pin));

	Blinker::on();
	m_LED1.on(random(256));
	m_startTime = millis();
};

//=======================================================
//Schweisslicht
//Mode 81
//Type n/a
// http://stummiforum.de/viewtopic.php?f=21&t=127899#p1446544
//=======================================================
Schweissen::Schweissen(uint8_t BaseChannel_, uint64_t minRandomPause_, uint64_t maxRandomPause_) :
	Blinker(BaseChannel_, 0, 0, 0, 0, 81),
	m_LED2(BaseChannel_ + 1, PWM_Set_On, 0, 0),
	m_LED3(BaseChannel_ + 2, PWM_Set_On, 0, 0),
	m_pausemin(minRandomPause_ * 10),
	m_pausemax(maxRandomPause_ * 10) {

}
Schweissen::~Schweissen(){
};

void Schweissen::process() {

	randomSeed(analogRead(RandomSeed_Pin));

	m_LED2.process();
	m_LED3.process();
	m_LED1.process();

	// do nothing if not active and not is running
	if (!m_active && !m_State)
		return;

	bool state_ = true;
	
	// if time to do something, do it
	if (millis() - m_startTime >= m_currentInterval) {
		m_startTime = millis();
		// Solange Flickertime gr�sser als 0 ist, schweissen
		if (m_flickertime > 0) {
			
			// Flickertime reduzieren
			m_flickertime--;

			// running State auf true
			m_State = true;

			// Flackerwert bestimmen
			uint8_t flicker_ = random(m_flickermin, m_flickermax);

			m_LED2.SetBrightness(flicker_);
			m_LED1.SetBrightness(flicker_);

			// Je l�nger geschweisst wird, desto heisser wird das Material
			m_glow++;

			m_LED3.SetBrightness(m_glow);

			// delay bestimmen
			m_currentInterval = random(m_minRandomTime, m_maxRandomTime);

			if (m_flickertime == 0) {
				// Es wird nicht mehr geschweisst
				m_LED2.SetBrightness(PWM_Set_Off);
				m_LED1.SetBrightness(PWM_Set_Off);
				
			};

		} // if (m_flickertime > 0)
		else if (m_glow > 0) {

			m_glow--;

			// Langsam ausgl�hen
			m_LED3.SetBrightness(m_glow);

			// delay bestimmen
			m_currentInterval = m_glowdelay;

			// Pause bis zum n�chsten Schweissen bestimmen
			if (m_glow == 0) {

				m_currentInterval = random(m_pausemin, m_pausemax);
			};

		} // else if (m_glow > 0)
		else if (m_glow == 0) {

			// Schweissvorgang neu starten
			m_flickertime = random(m_flickertimemin, m_flickertimemax);

			// Running State auf false
			m_State = false;
		};
	}; // if time to do something
	
};

void Schweissen::on() {
	
	Blinker::on();

	// running State auf true
	m_State = true;

	m_startTime = millis();

	m_glow = 0;

	// Flackerwert bestimmen
	uint8_t flicker_ = random(m_flickermin, m_flickermax);

	m_LED2.on(flicker_);
	m_LED1.on(flicker_);
	m_LED3.on(m_glow);

	// delay bestimmen
	m_currentInterval = random(m_minRandomTime, m_maxRandomTime);

	// Anzahl flicker
	m_flickertime = random(m_flickertimemin, m_flickertimemax);
};

void Schweissen::off() {
	m_active = false;
}


//=======================================================
//Feuer
//Mode 82
//Type n/a
// http://www.instructables.com/id/Realistic-Fire-Effect-with-Arduino-and-LEDs/?ALLSTEPS
//=======================================================
Feuer::Feuer(uint8_t BaseChannel_):
	Blinker(BaseChannel_, 0, 0, 0, 0, 82),
	m_LED2(BaseChannel_ + 1, PWM_Set_On, 0, 0),
	m_LED3(BaseChannel_ + 2, PWM_Set_On, 0, 0) {
};

Feuer::~Feuer(){
};

void Feuer::notifyCommand(uint8_t Port, uint8_t Value) {

	if (Port == m_BaseChannel) {
		// Einschalten
		if (Value == 1) {
			on();
		};

		// Ausschalten
		if (Value == 0) {
			off();
		};
	}

};

void Feuer::process() {
	m_LED2.process();
	m_LED3.process();
	m_LED1.process();

	// do nothing if not active and not is running
	if (!m_active)
		return;

	// if time to do something, do it
	if (millis() - m_startTime >= m_currentInterval) {
		m_startTime = millis();

		randomSeed(analogRead(RandomSeed_Pin));

		// delay bestimmen
		m_currentInterval = random(m_minRandomTime, m_maxRandomTime);

		// Helligkeit des Feuers bestimmen
		m_LED1.SetBrightness(random(256));  // gelb
		m_LED2.SetBrightness(random(256));  // rot
		m_LED3.SetBrightness(random(256));  // gelb
	};
};

void Feuer::off() {
	Blinker::off();
	m_active = false;
	m_LED1.off();
	m_LED2.off();
	m_LED3.off();
};

