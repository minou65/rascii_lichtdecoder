/*
Copyright (c) 2018 andy      <don_funk@bluewin.ch>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created by Andreas Zogg, September 26, 2018.

Blinker.h - Version 1.0.1

13.11.2018 Anstelle von StandardCplusPlus Library arduinostl verwenden
*/

#ifndef _BLINKER_h
#define _BLINKER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include <ArduinoSTL.h>
#include "accessories.h"
#include "LEDControl.h"

class Ausgang : public accessories {
private:
	LED Output;

	bool m_Active;
public:
	Ausgang(uint8_t BaseChannel_);
	virtual ~Ausgang();
	void notifyCommand(uint8_t Port, uint8_t Value);
	void on();
	void off();
	bool isOn() const;
};

//=======================================================
//Blinker mit 1 Lampen
//Mode 50
//Type n/a
//=======================================================
class Blinker : public accessories {
protected:
	LEDFader m_LED1;

	uint16_t m_timeOff;
	uint16_t m_timeOn;

	uint8_t m_Mode;

	bool m_active;
	bool m_State;

	unsigned long m_currentInterval;  // time till we change state
	unsigned long m_startTime;        // when we last changed state

public:
	Blinker(uint8_t BaseChannel_, uint16_t timeOff_, uint16_t timeOn_);
	Blinker(uint8_t BaseChannel_, uint16_t timeOff_, uint16_t timeOn_, uint8_t Mode_);
	Blinker(uint8_t BaseChannel_, uint16_t timeOff_, uint16_t timeOn_, uint8_t fadeUpTime_, uint8_t fadeDownTime_, uint8_t Mode_);
	virtual ~Blinker();
	void notifyCommand(uint8_t Port, uint8_t Value);
	void process();

	void on();
	void off();

	bool isOn() const;
};

//=======================================================
//Wechselblinker
//Mode 51
//Type n/a
//=======================================================
class Wechselblinker : public Blinker {
private:
	LEDFader m_LED2;

public:
	Wechselblinker(uint8_t BaseChannel_, uint16_t timeOff_, uint16_t timeOn_);
	Wechselblinker(uint8_t BaseChannel_, uint16_t timeOff_, uint16_t timeOn_, uint8_t fadeUpTime_, uint8_t fadeDownTime_);
	virtual ~Wechselblinker();
	void notifyCommand(uint8_t Port, uint8_t Value);
	void process();
	void off();
};

//=======================================================
//Lauflicht
//Mode 
//Type 
//=======================================================
class Lauflicht : public accessories {
private:
	std::vector<LEDFader*> m_LED;

	uint8_t m_NextStep;		// Welcher Ausgang als n�chstes eingeschaltet werden soll
	uint8_t m_LastStep;		// Welcher Ausgang als letzer eingeschaltet wurde
	uint8_t m_Anzahl;		// Wieviele Ausg�nge sollen verwendet werden

	uint8_t m_Mode;

	bool m_active;
	bool m_State;

	uint16_t m_timeOff;
	uint16_t m_timeOn;

	unsigned long m_currentInterval;  // time till we change state
	unsigned long m_startTime;        // when we last changed state

public:
	Lauflicht(uint8_t BaseChannel_, uint8_t Anzahl_, uint16_t timeOff_, uint16_t timeOn_, uint8_t Mode_);
	Lauflicht(uint8_t BaseChannel_, uint8_t Anzahl_, uint16_t timeOff_, uint16_t timeOn_, uint8_t fadeUpTime_, uint8_t fadeDownTime_, uint8_t Mode_);
	~Lauflicht();
	// void Init();
	void notifyCommand(uint8_t Port, uint8_t Value);
	void process();
};

//=======================================================
//Hausbeleuchtung
//Mode 60
//Type n/a
// http://forum.arduino.cc/index.php?topic=159117.15
// Zeiten in Sekunden
//=======================================================
class Hausbeleuchtung : public accessories {
private:
	std::vector<LED*> m_LED;

	uint16_t m_maxRandomTime;
	uint16_t m_minRandomTime;

	bool m_active;
	uint8_t m_Anzahl;		// Wieviele Ausg�nge sollen verwendet werden

	uint32_t m_currentInterval;  // time till we change state
	uint32_t m_startTime;        // when we last changed state

public:
	Hausbeleuchtung(uint8_t BaseChannel_, uint8_t Anzahl_, uint32_t minRandomTime_, uint32_t maxRandomTime_);
	~Hausbeleuchtung();
	void notifyCommand(uint8_t Port, uint8_t Value);
	void process();
};

//=======================================================
//Fernseher
//Mode 80
//Type n/a
// http://forum.arduino.cc/index.php?topic=159117.15
//=======================================================
class Fernseher : public Blinker {
public:
	Fernseher(uint8_t BaseChannel_);
	~Fernseher();
	void process();
	void on();
};

//=======================================================
//Schweisslicht
//Mode 81
//Type n/a
// http://stummiforum.de/viewtopic.php?f=21&t=127899#p1446544
//=======================================================
class Schweissen : public Blinker {
private:
	LEDFader m_LED2;		// blau
	LEDFader m_LED3;		// rot

	// Definition der Flackerzeit
	uint8_t m_minRandomTime = 15;
	uint8_t m_maxRandomTime = 125;

	// Definition des Ausgabewertes f�r das Schweisslicht
	uint8_t m_flickermin = 25;
	uint8_t m_flickermax = 255;

	// Definition wie viele Flackern vor Pause
	uint8_t m_flickertime = 0;
	uint8_t m_flickertimemin = 100;
	uint8_t m_flickertimemax = 255;

	// Startwert Gl�hen und Delay
	uint8_t m_glow = 0;
	uint8_t m_glowdelay = 15;

	// Definition L�nge der Pause
	uint16_t m_pausemin;
	uint16_t m_pausemax;

public:
	Schweissen(uint8_t BaseChannel_, uint64_t minRandomPause_, uint64_t maxRandomPause_);
	~Schweissen();
	void process();
	void on();
	void off();
};

//=======================================================
//Feuer
//Mode 82
//Type n/a
// http://www.instructables.com/id/Realistic-Fire-Effect-with-Arduino-and-LEDs/?ALLSTEPS
//=======================================================
class Feuer : public Blinker {
private:
	// LEDFader m_LED1;		// gelb
	LEDFader m_LED2;		// rot
	LEDFader m_LED3;		// gelb

	// Definition der Flackerzeit
	uint8_t m_minRandomTime = 15;
	uint8_t m_maxRandomTime = 125;

public:
	Feuer(uint8_t BaseChannel_);
	~Feuer();
	void notifyCommand(uint8_t Port, uint8_t Value);
	void process();
	void off();
};

#endif

