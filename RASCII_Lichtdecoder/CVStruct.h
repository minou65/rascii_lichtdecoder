/* 
Copyright (c) 2018 andy      <don_funk@bluewin.ch>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created by Andreas Zogg, September 26, 2018.

CVStruct.h - Version 1.0.0
*/

#ifndef _CVSTRUCT_h
#define _CVSTRUCT_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#define Default_Addr	255

#define CV_Addr			1
#define CV_Reset		8

struct CVPair {
	uint8_t CV;
	uint8_t Value;
};

uint8_t DefaultsCVIndex = 0;

const CVPair DefaultCVs[] PROGMEM = {
	
	// CV1, Board Address
	{ CV_Addr, 255},		

	// Chance  f�r defekte Lampe
	{60, 10},

	// Port 1
	{ 100, 40 },		// Mode
	{ 120, 0 },	// onTime
	{ 140, 0 },	// offTime
	{ 160, 0 },		// Multiplikator
	{ 180, 0 },	// onFade
	{ 200, 0 },	// offFade
	{ 220, 255 },	// Brightness Day
	{ 240, 100 },	// Brightness Nigth

	// Port 2
	{101, 40 },		// Mode
	{121, 0},		// onTime
	{141, 0},		// offTime
	{161, 0},		// Multiplikator
	{181, 0},		// onFade
	{201, 0},		// offFade
	{221, 255},		// Brightness Day
	{241, 100},		// Brightness Nigth

	// Port 3
	{ 102, 40 },		// Mode
	{ 122, 0 },	// onTime
	{ 142, 0 },	// offTime
	{ 162, 0 },		// Multiplikator
	{ 182, 0 },	// onFade
	{ 202, 0 },	// offFade
	{ 222, 255 },	// Brightness Day
	{ 242, 100 },	// Brightness Nigth

	// Port 4
	{ 103, 40 },		// Mode
	{ 123, 0 },		// onTime
	{ 143, 0 },		// offTime
	{ 163, 0 },		// Multiplikator
	{ 183, 0 },		// onFade
	{ 203, 0 },		// offFade
	{ 223, 255 },		// Brightness Day
	{ 243, 100 },		// Brightness Nigth

	// Port 5
	{ 104, 40 },		// Mode
	{ 124, 0 },	// onTime
	{ 144, 0 },	// offTime
	{ 164, 0 },		// Multiplikator
	{ 184, 0 },	// onFade
	{ 204, 0 },	// offFade
	{ 224, 255 },	// Brightness Day
	{ 244, 100 },	// Brightness Nigth


	// Port 6
	{ 105, 40 },		// Mode
	{ 125, 0 },		// onTime
	{ 145, 0 },		// offTime
	{ 165, 0 },		// Multiplikator
	{ 185, 0 },		// onFade
	{ 205, 0 },		// offFade
	{ 225, 255 },		// Brightness Day
	{ 245, 100 },		// Brightness Nigth

	// Port 7
	{ 106, 40 },		// Mode
	{ 126, 0 },	// onTime
	{ 146, 0 },	// offTime
	{ 166, 0 },		// Multiplikator
	{ 186, 0 },	// onFade
	{ 206, 0 },	// offFade
	{ 226, 255 },	// Brightness Day
	{ 246, 100 },	// Brightness Nigth

	// Port 8
	{ 107, 40 },		// Mode
	{ 127, 0 },		// onTime
	{ 147, 0 },		// offTime
	{ 167, 0 },		// Multiplikator
	{ 187, 0 },		// onFade
	{ 207, 0 },		// offFade
	{ 227, 255 },		// Brightness Day
	{ 247, 100 },		// Brightness Nigth

	// Port 9
	{ 108, 40 },		// Mode
	{ 128, 0 },	// onTime
	{ 148, 0 },	// offTime
	{ 168, 0 },		// Multiplikator
	{ 188, 0 },	// onFade
	{ 208, 0 },	// offFade
	{ 228, 255 },	// Brightness Day
	{ 248, 100 },	// Brightness Nigth

	// Port 10
	{ 109, 40 },		// Mode
	{ 129, 0 },		// onTime
	{ 149, 0 },		// offTime
	{ 169, 0 },		// Multiplikator
	{ 189, 0 },		// onFade
	{ 209, 0 },		// offFade
	{ 229, 255 },		// Brightness Day
	{ 249, 100 },		// Brightness Nigth

	// Port 11
	{ 110, 40 },		// Mode
	{ 130, 0 },	// onTime
	{ 150, 0 },	// offTime
	{ 170, 0 },		// Multiplikator
	{ 190, 0 },	// onFade
	{ 210, 0 },	// offFade
	{ 230, 255 },	// Brightness Day
	{ 250, 100 },	// Brightness Nigth

	// Port 12
	{ 111, 40 },		// Mode
	{ 131, 0 },	// onTime
	{ 151, 0 },	// offTime
	{ 171, 0 },		// Multiplikator
	{ 191, 0 },	// onFade
	{ 211, 0 },	// offFade
	{ 231, 255 },	// Brightness Day
	{ 251, 100 },	// Brightness Nigth

	// Port 13
	{ 112, 40 },		// Mode
	{ 132, 0 },	// onTime
	{ 152, 0 },	// offTime
	{ 172, 0 },		// Multiplikator
	{ 192, 0 },	// onFade
	{ 212, 0 },	// offFade
	{ 232, 255 },	// Brightness Day
	{ 252, 100 },	// Brightness Nigth


	// Port 14
	{ 113, 40 },		// Mode
	{ 133, 0 },	// onTime
	{ 153, 0 },	// offTime
	{ 173, 0 },		// Multiplikator
	{ 193, 0 },	// onFade
	{ 213, 0 },	// offFade
	{ 233, 255 },	// Brightness Day
	{ 253, 100 },	// Brightness Nigth

	// Port 15
	{ 114, 40 },		// Mode
	{ 134, 0 },	// onTime
	{ 154, 0 },	// offTime
	{ 174, 0 },		// Multiplikator
	{ 194, 0 },	// onFade
	{ 214, 0 },	// offFade
	{ 234, 255 },	// Brightness Day
	{ 254, 100 },	// Brightness Nigth

	// Port 16
	{ 115, 40 }, // Mode
	{ 135, 0 },	// onTime
	{ 155, 0 },	// offTime
	{ 175, 0 },	// Multiplikator
	{ 195, 0 },	// onFade
	{ 215, 0 },	// offFade
	{ 235, 255 },	// Brightness Day
	{ 255, 100 },	// Brightness Nigth
};

#endif

