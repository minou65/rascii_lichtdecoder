/*
Copyright (c) 2018 andy      <don_funk@bluewin.ch>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created by Andreas Zogg, September 26, 2018.

LEDControl.cpp - Version 1.0.0
*/

// #define LEDCONTRL_DEBUG

#include "LEDControl.h"

// Um wieviel soll das fading erh�ht werden
#define PWM_FadeRate 1

// PWM Treiber
PCA9622DR PLED01;


// ===========================================
// LED
// ===========================================
LED::LED(uint8_t Channel_) :
	m_Channel(Channel_),
	m_active(false) {

#ifdef LEDCONTRL_DEBUG
	Serial.print(F("LED::LED, Channel = ")); Serial.println(Channel_);
#endif // LEDCONTRL_DEBUG

	if (m_Channel > PWM_MAXCHANNELS)
		m_Channel = PWM_MAXCHANNELS;

	PLED01.SetChannelOff(m_Channel);
}
LED::~LED() {
};

// virtuelle Funktion
void LED::process() {
};

void LED::on() {
#ifdef LEDCONTRL_DEBUG
	Serial.print(F("LED::on, Channel = ")); Serial.println(m_Channel);
#endif // LEDCONTRL_DEBUG

	m_active = true;
	PLED01.SetChannelOn(m_Channel);
};

void LED::off() {
#ifdef LEDCONTRL_DEBUG
	Serial.print(F("LED::off, Channel = ")); Serial.println(m_Channel);
#endif // LEDCONTRL_DEBUG
	m_active = false;
	PLED01.SetChannelOff(m_Channel);
};

bool LED::isOn() {
	return m_active;
};

// ===========================================
// LEDFader
// ===========================================
LEDFader::LEDFader(uint8_t Channel_) :
	LED(Channel_),
	m_Brightness(PWM_Set_On),
	m_StartTime(millis()) {

	SetFadeTime(500, 500);
	SetFadeMultiplikator(1);
	m_Current = PWM_Set_Off;
	m_Target = PWM_Set_Off;

};

LEDFader::LEDFader(uint8_t Channel_, uint8_t Brightness_, uint16_t fadeUpTime_, uint16_t fadeDownTime_) :
	LED(Channel_),
	m_Brightness(Brightness_),
	m_StartTime(millis()) {

	SetFadeTime(fadeUpTime_, fadeDownTime_);
	SetFadeMultiplikator(1);
	m_Current = PWM_Set_Off;
	m_Target = PWM_Set_Off;
};

void LEDFader::process() {
	LED::process();

	int16_t direction_;
	uint64_t now_ = millis();

	// if we have to do somhting, then do it
	if (m_Current != m_Target) {

		direction_ = m_Target - m_Current;
		// Fade up
		if (direction_ > 0) {

			if (m_fadeUpRate == 0) {
				// Set direct the Brightness
				m_Current = m_Target;
			}
			else if (now_ - m_StartTime >= m_fadeUpIntervall * m_FadeMultiplikator) {
				// fading
				m_Current = m_Current + m_fadeUpRate;
				if (m_Current > m_Target)
					m_Current = m_Target;
				m_StartTime = now_;
			};
		}
		// Fade down
		else if (direction_ < 0) {
			if (m_fadeDownRate == 0) {
				// Set direct the Brightness
				m_Current = m_Target;
			}
			else if (now_ - m_StartTime >= m_fadeDownIntervall  * m_FadeMultiplikator) {
				// fading
				m_Current = m_Current - m_fadeDownRate;
				if (m_Current < m_Target)
					m_Current = m_Target;
				m_StartTime = now_;
			};
		};
	};


	// Set the Channel
	if (m_Current == PWM_Set_Off) {
		PLED01.SetChannelOff(m_Channel);
	}
	else if (m_Current == PWM_Set_On) {
		PLED01.SetChannelOn(m_Channel);
	}
	else {
		//PLED01.SetChannelPWM(m_Channel, m_Current * 16);  // PCA9685
		PLED01.SetChannelPWM(m_Channel, m_Current);
	};

};

void LEDFader::on() {
	m_active = true;
	m_Target = m_Brightness;
};

void LEDFader::on(uint8_t Brightness_) {
	m_active = true;
	SetBrightness(Brightness_);
};

void LEDFader::off() {
	m_active = false;
	m_Target = PWM_Set_Off;
};

void LEDFader::SetPercent(uint8_t Percent_, bool Hardset_) {
	m_active = true;
	SetBrightness(((uint32_t)Percent_ * 255) / 100, Hardset_);
};

void LEDFader::SetBrightness(uint8_t Value_, bool Hardset_) {

	if (Value_ >= m_Brightness)
		m_Target = m_Brightness;
	else
		m_Target = Value_;

	if (isOn() && Hardset_) {
		m_Current = m_Target;
	}
};

void LEDFader::SetMaxBrightness(uint8_t Max_) {

	m_Brightness = Max_;

	if (m_Target != 0)
		m_Target = m_Brightness;
};

void LEDFader::SetFadeTime(uint16_t fadeUpTime_, uint16_t fadeDownTime_) {
	// kleinst m�gliches Intervall ist 10ms
	// maximale Schritte sind 255, je mehr Schritte umso fl�ssiger ist das fading
	// durch erh�hen von fadeIntervall k�nnen l�ngere Zeiten erreicht werden
	uint16_t  fadeAmount_;

	fadeAmount_ = 0;
	if (fadeUpTime_ > 0)
		fadeAmount_ = 255 / (fadeUpTime_ / m_fadeUpIntervall);
	m_fadeUpRate = fadeAmount_;

#ifdef LEDCONTROL_DEBUG_MSG
	Serial.print(F("LEDFader::SetFadeTime; Channel: ")); Serial.println(m_Channel);
	Serial.print(F("m_fadeUpIntervall: ")); Serial.println(m_fadeUpIntervall);
	Serial.print(F("fadeUpTime:        ")); Serial.println(fadeUpTime_);
	Serial.print(F("m_fadeUpRate:      ")); Serial.println(m_fadeUpRate);
#endif

	fadeAmount_ = 0;
	if (fadeDownTime_ > 0)
		fadeAmount_ = 255 / (fadeDownTime_ / m_fadeDownIntervall);
	m_fadeDownRate = fadeAmount_;
};

void LEDFader::SetFadeTime(uint16_t fadeUpTime_, uint16_t fadeDownTime_, uint16_t fadeUpIntervall_, uint16_t fadeDownIntervall_) {
	m_fadeUpIntervall = fadeUpIntervall_;
	m_fadeDownIntervall = fadeDownIntervall_;

	// minimal Wert ist 10
	if (m_fadeUpIntervall < 10)
		m_fadeUpIntervall = 10;
	if (m_fadeDownIntervall < 10)
		m_fadeDownIntervall = 10;

	SetFadeTime(fadeUpTime_, fadeDownTime_);
};

void LEDFader::SetFadeMultiplikator(uint8_t Value_) {
	m_FadeMultiplikator = Value_;
};

uint8_t LEDFader::GetCurrentBrightness() {
	return m_Current;
};

bool LEDFader::isDark() {
	return GetCurrentBrightness() == PWM_Set_Off;
};


// ===========================================
// Natrium
// ===========================================
Natrium::Natrium(const uint8_t Channel_, const bool  Defekt_, uint8_t fadeOnIntervall_, uint8_t fadeOffIntervall_) :
	LEDFader(Channel_),
	m_Defekt(Defekt_) {

}
Natrium::~Natrium() {
};

void Natrium::process() {

	LEDFader::process();

	unsigned long now_ = millis();

	if (now_ - m_startTime >= m_currentInterval) {

		// Lampe gl�ht und baut nun Betriebsdruck auf
		if (m_CurrentState == NATRIUM_VorGluehen) {
			m_CurrentState = NATRIUM_Betriebsdruck;

			// Wenn defekt, dann Ausfallzeit berechnen
			if (m_Defekt) {
				m_DefektInterval = random(NATRIUM_DefektIntervall_min, NATRIUM_DefektIntervall_max);
				m_DefektStartTime = now_;
			}

		}

		// Lampe baut Betriebsdruck auf
		else if ((m_CurrentState == NATRIUM_Betriebsdruck) &&
			(GetCurrentBrightness() != PWM_Set_On)) {

			SetBrightness(GetCurrentBrightness() + 1);

		}

		// Lampe hat Betriebsdruck erreicht
		else if ((m_CurrentState == NATRIUM_Betriebsdruck) &&
			(GetCurrentBrightness() == PWM_Set_On)) {

			m_CurrentState = NATRIUM_ON;
		}

		// Lampe Gl�ht aus
		else if ((m_CurrentState == NATRIUM_AusGluehen) &&
			(GetCurrentBrightness() != PWM_Set_Off) &&
			m_active) {

			SetBrightness(GetCurrentBrightness() - 1);

		}

		// Lampe ist Ausgegl�ht
		else if ((m_CurrentState == NATRIUM_AusGluehen) &&
			(GetCurrentBrightness() == PWM_Set_Off) &&
			m_active) {

			m_CurrentState = NATRIUM_OFF;
		}

		// Lampe ist defekt und f�llt nun aus
		else if ((m_CurrentState == NATRIUM_Betriebsdruck || m_CurrentState == NATRIUM_ON) &&
			(m_active) && (m_Defekt) &&
			(now_ - m_DefektStartTime >= m_DefektInterval)) {

			// Ausschalten, Lampe gl�ht noch
			uint8_t Percent_ = random(m_randomMin, m_randomMax);
			SetPercent(Percent_, true);

			m_CurrentState = NATRIUM_AusGluehen;

			// Einschaltzeit berechnen
			m_DefektInterval = random(NATRIUM_DefektIntervall_min, NATRIUM_DefektIntervall_max);
			m_DefektStartTime = now_;
		}

		// Lampe ist defekt und schaltet nun wieder ein
		else if ((m_CurrentState == NATRIUM_AusGluehen || m_CurrentState == NATRIUM_OFF) &&
			m_active && m_Defekt &&
			(now_ - m_DefektStartTime >= m_DefektInterval)) {

			// Vorgl�hen 
			uint8_t Percent_ = random(m_randomMin, m_randomMax);
			SetPercent(Percent_, true);

			m_CurrentState = NATRIUM_VorGluehen;
		}

		m_startTime = now_;
	}
};

void Natrium::on() {
	m_active = true;

	// Vorgl�hen 
	uint8_t Percent_ = random(m_randomMin, m_randomMax);
	SetPercent(Percent_, true);

	m_CurrentState = NATRIUM_VorGluehen;
	m_startTime = millis();
	m_currentInterval = 1000;
};

void Natrium::off() {
	m_active = false;

	// Ausschalten, Lampe gl�ht noch
	uint8_t Percent_ = random(m_randomMin, m_randomMax);
	SetPercent(Percent_, true);

	m_CurrentState = NATRIUM_AusGluehen;
	m_startTime = millis();
	m_currentInterval = 1000;
};

// ===========================================
// Neon
// ===========================================
Neon::Neon(const uint8_t Channel_, const bool  Defekt_) :
	LED(Channel_),
	m_Defekt(Defekt_) {
}
Neon::~Neon() {
};

void Neon::process() {

	LED::process();

	// do nothing if not active and is not running
	if (!m_active)
		return;

#ifdef LEDCONTRL_DEBUG
	Serial.print(F("Neon::process, Channel = ")); Serial.println(m_Channel);
#endif // LEDCONTRL_DEBUG

	unsigned long now_ = millis();

	// if time to do something, do it
	if (now_ - m_startTime >= m_currentInterval) {

		// Wir m�ssen blinken
		if (m_FlashCount > 0) {

			if (m_State) {
				// Einsachltverz�gerung
				m_currentInterval = random(300, 500);
				// Lampe aus
				PLED01.SetChannelOff(m_Channel);

			}
			else {

#ifdef LEDCONTRL_DEBUG
				Serial.print(F("Neon::process, Disable Channel = ")); Serial.println(m_Channel);
#endif // LEDCONTRL_DEBUG

				// Ausschaltverz�gerung
				m_currentInterval = random(200, 400);
				// Lampe ein
				PLED01.SetChannelOn(m_Channel);


				// SetPercent(100);
			}

			// Defekte Lampe, Einschaltverz�gern darf gr�sser sein
			if (m_State && m_Defekt)
				m_currentInterval = random(200, 2000);

			// Blinker runterz�hlen wenn Lampe nicht defekt ist
			if (!m_Defekt)
				m_FlashCount--;

			m_State = !m_State;

		}
		else {

#ifdef LEDCONTRL_DEBUG
			Serial.print(F("Neon::process, Enable Channel = ")); Serial.println(m_Channel);
#endif // LEDCONTRL_DEBUG

			// Wir m�ssen nicht mehr blinken, Lampe ein
			PLED01.SetChannelOn(m_Channel);

		}

		m_startTime = now_;
	};

};

void Neon::on() {

#ifdef LEDCONTRL_DEBUG
	Serial.print(F("Neon::on, Channel = ")); Serial.println(m_Channel);
#endif // LEDCONTRL_DEBUG

	m_active = true;
	m_State = false;

	// Blinker festlegen
	m_FlashCount = random(1, 10);

	// Defekte Lampe dann muss m_Flashcount auf jedenfall gr�sser als 0 sein
	if (m_Defekt)
		m_FlashCount = 1;

	m_startTime = millis();

	// Wie lange soll gewartet werden bis zum ersten Einschalten
	m_currentInterval = random(400, 600);

};




