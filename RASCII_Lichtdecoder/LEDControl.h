/*
Copyright (c) 2018 andy      <don_funk@bluewin.ch>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created by Andreas Zogg, September 26, 2018.

LEDControl.h - Version 1.0.0
*/

#ifndef _LEDCONTROL_h
#define _LEDCONTROL_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include <Wire.h>
#include "PCA9622DR.h"

// Values for On and Off
#define PWM_Set_On 255
#define PWM_Set_Off 0

#define PWM_MAXCHANNELS 16


// Stati f�r Natrium Einschalt und Ausschaltprozess
#define NATRIUM_VorGluehen 1
#define NATRIUM_Betriebsdruck 2
#define NATRIUM_ON 3
#define NATRIUM_AusGluehen 4
#define NATRIUM_OFF 5

#define NATRIUM_FadeMultiplikator 500
#define NATRIUM_DefektIntervall_min 5000
#define NATRIUM_DefektIntervall_max	20000

// PWM Treiber
extern PCA9622DR PLED01;

// Channel wird als normaler Ausgang f�r eine LED geschaltet
class LED {

protected:
	int m_Channel;						 // the number of the LED pin
	bool m_active;

public:
	LED(uint8_t Channel_);
	virtual ~LED();

	// Muss regelm�ssig aufgerufen werden
	virtual void process();

	void on();
	void off();
	bool isOn();
};

// Channel erlaubt das faden einer LED
class LEDFader : public LED {
private:
	uint16_t m_fadeUpIntervall = 10;
	uint16_t m_fadeDownIntervall = 10;

	// Anzahl Steps um die die Helligkeit der LED pro Intervall erh�ht wird 1-255
	// Ist der Wert 0 wird Helligkeit direkt auf Target gesetzt
	uint8_t m_fadeUpRate;
	uint8_t m_fadeDownRate;

	// Um besonders hohe Zeiten zu erreichen wird Intervall mit diesem Wert multipliziert. Default ist 1
	uint8_t m_FadeMultiplikator;

	uint64_t m_StartTime;

	uint8_t m_Brightness;					// On Wert
	int16_t m_Current;					    // Aktuelle Wert f�r LED beim faden
	uint8_t m_Target;					    // Wert, welcher eingestellt werden soll

public:
	LEDFader(uint8_t Channel_);
	LEDFader(uint8_t Channel_, uint8_t Brightness_, uint16_t fadeUpTime_ = 1000, uint16_t fadeDownTime_ = 1000);

	// Muss regelm�ssig aufgerufen werden
	void process();

	void on();
	void on(uint8_t Brightness_);
	void off();

	// Brightness in Prozent 0 - 100; , wenn Hardset == true dann wird der Wert direkt gesetzt ohne fading
	void SetPercent(uint8_t Percent_, bool Hardset_ = false);
	// Brightness von 0 - 255, wenn Hardset == true dann wird der Wert direkt gesetzt ohne fading
	void SetBrightness(uint8_t Brightness_, bool Hardset_ = false);
	// Setzen der maximalen Helligkeit
	void SetMaxBrightness(uint8_t Max_);

	// Time in ms
	void SetFadeTime(uint16_t fadeUpTime_, uint16_t fadeDownTime_);
	// Time in ms, zudem kann das Intevall angepasst werden, kleinst m�glicher Wert ist 10ms
	void SetFadeTime(uint16_t fadeUpTime_, uint16_t fadeDownTime_, uint16_t fadeUpIntervall_, uint16_t fadeDownIntervall_);
	// Mit diser funktion kann das Intervall zus�tzlich erh�ht werden. Sollte nicht mehr verwendet werden
	void SetFadeMultiplikator(uint8_t Value_);
	// Liefert die aktuelle helligkeit
	uint8_t GetCurrentBrightness();
	// Pr�ft ob die LED ganz Dunkel ist
	bool isDark();
};

// Simuliert eine Natriumdampflampe
class Natrium : public LEDFader {
private:
	const uint8_t m_randomMax = 15;		// Definition in Prozent wie stark die Lampe beim
	const uint8_t m_randomMin = 5;		// Ein- und Ausschalten noch leuchten soll
										// Danach dimmt die Lampe zu 100% oder zu 0%

	uint32_t m_startTime;
	uint32_t m_currentInterval;

	bool m_Defekt = false;
	uint32_t m_DefektStartTime;
	uint32_t m_DefektInterval;

	uint8_t	m_CurrentState;

public:

	Natrium(const uint8_t Channel_, const bool  Defekt_, uint8_t fadeOnIntervall_ = 10, uint8_t fadeOffIntervall_ = 10);
	virtual ~Natrium();

	void process();

	void on();
	void off();

};

// Simuliert ein Neonr�hre
class Neon : public LED {
private:
	bool m_Defekt = false;
	bool m_State = false;

	uint8_t m_FlashCount;			// Wie oft soll beim einschalten geblinkt werden

	uint32_t m_startTime;
	uint32_t m_currentInterval;

public:
	Neon(const uint8_t Channel_, const bool Defekt_);
	virtual ~Neon();

	void process();

	void on();

};




#endif

