/*
Copyright (c) 2018 andy      <don_funk@bluewin.ch>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created by Andreas Zogg, September 26, 2018.

Lamps.cpp - Version 1.0.0
*/

#include "Lamps.h"

using namespace std;

#define RandomSeed_Pin	7

//=======================================================
//NeonLampen
//Mode 61
//Type n/a
// http://forum.arduino.cc/index.php?topic=159117.15
// 
//=======================================================
NeonLampen::NeonLampen(uint8_t BaseChannel_, uint8_t Anzahl_, uint8_t Chance_) :
	accessories(BaseChannel_),
	m_Active(false),
	m_Anzahl(Anzahl_),
	m_Chance(Chance_) {

	uint8_t DefekteLampe_ = 0;
	bool Defekt_ = false;

	randomSeed(analogRead(RandomSeed_Pin));
	// Chance_
	// Prozentuale chance auf defekte Lampe: 
	// f�r 50:50 den Wert 1. f�r 33:66 den Wert 2, etc.
	// wenn Null dann gibt es keine defekte Lampe
	// wenn 255, dann gibt es immer eine defekte Lampe
	if (((m_Chance >= 1) && (random(0, 1 + m_Chance) == 1)) || (m_Chance == 255)) {
		// Wir haben eine defekte Lampe
		Defekt_ = true;
		// Welche Lampe ist defekt?
		DefekteLampe_ = random(0, m_Anzahl);
	};

	// Anzahl Lampen erstellen
	for (int i_ = 0; i_ < m_Anzahl; i_++) {
		if (Defekt_ && (i_ == DefekteLampe_))
			m_Lampen.push_back(new Neon(m_BaseChannel + i_, true));
		else
			m_Lampen.push_back(new Neon(m_BaseChannel + i_, false));
	};
};

NeonLampen::~NeonLampen() {
	for (int i = 0; i < m_Lampen.size(); i++) {
		delete m_Lampen[i];
	};

	// Gr�sse von Decoder auf 0 setzen
	m_Lampen.clear();

	// Speicher von decoder freigeben
	vector<Neon*>().swap(m_Lampen);
};

void NeonLampen::notifyCommand(uint8_t Port, uint8_t Value) {

	if (Port == m_BaseChannel) {
		// Einschalten
		if (Value == 1) {
			on();
		};

		// Ausschalten
		if (Value == 0) {
			off();
		};

	}

};

void NeonLampen::process() {

	for (vector<Neon*>::const_iterator Lampe_ = m_Lampen.begin(); Lampe_ != m_Lampen.end(); Lampe_++) {
		(*Lampe_)->process();
	};

};

void NeonLampen::on() {

	m_Active = true;
	for (vector<Neon*>::const_iterator Lampe_ = m_Lampen.begin(); Lampe_ != m_Lampen.end(); Lampe_++) {
		(*Lampe_)->on();
	};
};

void NeonLampen::off() {

	m_Active = false;
	for (vector<Neon*>::const_iterator Lampe_ = m_Lampen.begin(); Lampe_ != m_Lampen.end(); Lampe_++) {
		(*Lampe_)->off();
	};
}

//=======================================================
//NatriumLampen
//Mode 62
//Type n/a
// 
//=======================================================
NatriumLampen::NatriumLampen(uint8_t BaseChannel_, uint8_t Anzahl_, uint8_t Chance_, uint8_t fadeOnIntervall_ = 10, uint8_t fadeOffIntervall_ = 10) :
	accessories(BaseChannel_),
	m_Anzahl(Anzahl_),
	m_Active(false),
	m_Chance(Chance_),
	m_fadeOnIntervall(fadeOnIntervall_),
	m_fadeOffIntervall(fadeOffIntervall_) {

	uint8_t DefekteLampe_ = 0;
	bool Defekt_ = false;

	if (!m_fadeOnIntervall)
		m_fadeOnIntervall = 1;

	if (!m_fadeOffIntervall)
		m_fadeOffIntervall = 1;

	randomSeed(analogRead(RandomSeed_Pin));
	// Chance_
	// Prozentuale chance auf defekte Lampe: 
	// f�r 50:50 den Wert 1. f�r 33:66 den Wert 2, etc.
	// wenn Null dann gibt es keine defekte Lampe
	// wenn 255, dann gibt es immer eine defekte Lampe
	if (((m_Chance >= 1) && (random(0, 1 + m_Chance) == 1)) || (m_Chance == 255)) {
		// Wir haben eine defekte Lampe
		Defekt_ = true;
		// Welche Lampe ist defekt?
		DefekteLampe_ = random(0, m_Anzahl);
	};

	// Anzahl Lampen erstellen
	for (int i_ = 0; i_ < m_Anzahl; i_++) {

		if (Defekt_ && (i_ == DefekteLampe_))
			m_Lampen.push_back(new Natrium(m_BaseChannel + i_, true, m_fadeOnIntervall, m_fadeOffIntervall));
		else
			m_Lampen.push_back(new Natrium(m_BaseChannel + i_, false, m_fadeOnIntervall, m_fadeOffIntervall));

	};

}

NatriumLampen::~NatriumLampen() {
	for (int i = 0; i < m_Lampen.size(); i++) {
		delete m_Lampen[i];
	};

	// Gr�sse von Decoder auf 0 setzen
	m_Lampen.clear();

	// Speicher von decoder freigeben
	vector<Natrium*>().swap(m_Lampen);
};

void NatriumLampen::notifyCommand(uint8_t Port, uint8_t Value) {

	if (Port == m_BaseChannel) {
		// Einschalten
		if (Value == 1) {
			on();
		};

		// Ausschalten
		if (Value == 0) {
			off();
		};
	};
}


void NatriumLampen::process() {
	for (vector<Natrium*>::const_iterator Lampe_ = m_Lampen.begin(); Lampe_ != m_Lampen.end(); Lampe_++) {
		(*Lampe_)->process();
	};
};

void NatriumLampen::on() {
	m_Active = true;

	// Lampen leuchten mit 15 - 25 % beim einschalten, danach auf 100%
	for (vector<Natrium*>::const_iterator Lampe_ = m_Lampen.begin(); Lampe_ != m_Lampen.end(); Lampe_++) {
		(*Lampe_)->on();
	};
};

void NatriumLampen::off() {
	m_Active = false;

	// Lampen auschalten, sie leuchten noch mit 15 -25% und leuchten dann langsam aus.
	for (vector<Natrium*>::const_iterator Lampe_ = m_Lampen.begin(); Lampe_ != m_Lampen.end(); Lampe_++) {
		(*Lampe_)->off();
	};
};

