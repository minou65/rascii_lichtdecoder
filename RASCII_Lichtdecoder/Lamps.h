/*
Copyright (c) 2018 andy      <don_funk@bluewin.ch>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created by Andreas Zogg, September 26, 2018.

Lamps.h - Version 1.0.1

13.11.2018 Anstelle von StandardCplusPlus Library arduinostl verwenden
*/

#ifndef _LAMPS_h
#define _LAMPS_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include <ArduinoSTL.h>
#include "accessories.h"
#include "LEDControl.h"

//=======================================================
//NeonLampen
//Mode 61
//Type n/a
// http://forum.arduino.cc/index.php?topic=159117.15
// 
//=======================================================
class NeonLampen : public accessories {
private:
	bool m_Chance = 0;						// Prozentuale chance auf defekte Lampe: 
											// f�r 50:50 den Wert 1. f�r 33:66 den Wert 2, etc.
											// wenn 0 dann gibt es keine defekte R�hre
											// wenn 255, dann gibt es immer eine defekte R�hre

	bool m_Active;

	std::vector<Neon*> m_Lampen;
	const uint8_t m_Anzahl;

public:
	NeonLampen(uint8_t BaseChannel_, const uint8_t Anzahl_, const uint8_t DefekteNeonChance_);
	virtual ~NeonLampen();
	void notifyCommand(uint8_t Port, uint8_t Value);
	void process();

	void on();
	void off();
};

//=======================================================
//NatriumLampen
//Mode 62
//Type n/a
// 
//=======================================================
class NatriumLampen :public accessories {
private:
	uint8_t m_Chance = 0;					// Prozentuale chance auf defekte Lampe: 
											// f�r 50:50 den Wert 1. f�r 33:66 den Wert 2, etc.
											// wenn 0 dann gibt es keine defekte R�hre
											// wenn 255, dann gibt es immer eine defekte R�hre

	bool m_Active;

	uint8_t m_fadeOnIntervall;
	uint8_t m_fadeOffIntervall;

	std::vector<Natrium*> m_Lampen;
	const uint8_t m_Anzahl;

public:

	NatriumLampen(uint8_t BaseChannel_, uint8_t Anzahl_, uint8_t Chance_, uint8_t fadeOnIntervall_, uint8_t fadeOffIntervall_);
	virtual ~NatriumLampen();
	void notifyCommand(uint8_t Port, uint8_t Value);
	void process();

	void on();
	void off();
};


#endif

