/*
Copyright (c) 2018 andy      <don_funk@bluewin.ch>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created by Andreas Zogg, September 26, 2018.

Lichtdecoder.ino - Version 1.0.0
*/


using namespace std;


#include <ArduinoSTL.h>
#include <Wire.h>
#include <EEPROM.h>
#include <vector>
#include "CVStruct.h"
#include "RASCII.h"
#include "accessories.h"
#include "Blinker.h"
#include "Lamps.h"


#define LightingFunctions
//#define FORCE_RESET_FACTORY_DEFAULT_CV

 #define DEBUG_INIT
 // #define DEBUG_MSG

#ifdef DEBUG_INIT
#include <MemoryFree.h>
#endif // DEBUG_MSG

RASCII rascii(false);
uint8_t myaddr = 255;

// Liste mit allen Decoderobjekten (Apps)
vector<accessories*> decoder;

uint8_t GetCV(uint8_t CV_) {
	return EEPROM.read(CV_);
}

bool validCV(uint8_t CV_) {
	bool valid_ = false;
	for (uint8_t i_; i_ < sizeof(DefaultCVs) / sizeof(CVPair); i_++) {
		uint8_t _cv = pgm_read_word(&DefaultCVs[i_].CV);
		if (_cv == CV_) {
			valid_ = true;
		}
	}
#ifdef DEBUG_MSG
	Serial.print(F("CV ")); Serial.print(CV_); Serial.print(F(" is ")); Serial.println(valid_);
#endif 
	return valid_;
};

uint8_t SetCV(uint8_t CV_, uint8_t value_) {
	EEPROM.write(CV_, value_);
	//if ((EEPROM.read(CV_) != value_) && validCV(CV_)) {
	//	EEPROM.write(CV_, value_);
	//	notify_CVChange(CV_, value_);
	//}
	return EEPROM.read(CV_);
}

void destroyDecoder() {
	// Decoderobjekte in decoder l�schen
	for (vector<accessories*>::const_iterator it = decoder.begin(); it != decoder.end(); it++) {
		delete *it;
	};

	// Gr�sse von Decoder auf 0 setzen
	decoder.clear();
	decoder.resize(0);

	// Speicher von decoder freigeben
	// vector<accessories*>(decoder).swap(decoder);
};

void initDecoder() {

#ifdef DEBUG_INIT
	Serial.println(F("void::initDecoder"));
#endif

	// PWM Chip sperren
	digitalWrite(OEPin, HIGH);

	myaddr = GetCV(CV_Addr);

#ifdef DEBUG_INIT
	uint16_t freeMemory_ = freeMemory();
	Serial.println(F("========================================="));
	Serial.print(F("Init Decoder: Address: ")); Serial.println(GetCV(CV_Addr), DEC);
	Serial.print(F("  Free Memory, before Init: ")); Serial.println(freeMemory_);
	Serial.println(F(" "));
#endif  

	for (uint8_t Port_ = 0; Port_ <= 15; Port_++) {

		// diverse CV's auslesen
		uint8_t CV_Mode_ = GetCV(100 + Port_);

		uint8_t CV_onTime_ = GetCV(120 + Port_);
		uint8_t CV_offTime_ = GetCV(140 + Port_);
		uint8_t	CV_Multiplikator_ = GetCV(160 + Port_);

		uint8_t CV_onFade_ = GetCV(180 + Port_);
		uint8_t CV_offFade_ = GetCV(200 + Port_);

		uint8_t CV_BrightnessDay_ = GetCV(220 + Port_);
		uint8_t CV_BrightnessNight_ = GetCV(240 + Port_);

		// Get the Numbers of Ports that should be used
		uint8_t NumberOfPorts_ = GetCV(100 + Port_ + 1);
		if (NumberOfPorts_ >= 20)
			NumberOfPorts_ = 1;
		if ((NumberOfPorts_ == 0) || (NumberOfPorts_ > 16))
			NumberOfPorts_ = 6;

		uint8_t Chance_ = GetCV(60);

#ifdef DEBUG_INIT
		Serial.print(F("  Port: ")); Serial.print(Port_ + 1, DEC);  
		Serial.print(F(", Mode: ")); 
		Serial.print(CV_Mode_, DEC);
		Serial.print(F(", FadeUp: ")); Serial.print(CV_onFade_);
		Serial.print(F(", FadeDown: ")); Serial.print(CV_offFade_);
		Serial.print(F(", TimeOn: ")); Serial.print(CV_onTime_);
		Serial.print(F(", TimeOff: ")); Serial.print(CV_offTime_);
		Serial.print(F(", Multiplikator: ")); Serial.println(CV_Multiplikator_);
#endif // DEBUG_INIT

		// Einrichten des Ports
		switch (CV_Mode_) {

#ifdef LightingFunctions
		case 40:			// einfacher Ausgang
			decoder.push_back(new Ausgang(Port_ + 1));
			break;

		case 50:			// Blinker
			decoder.push_back(new Blinker(Port_ + 1, CV_Multiplikator_ * CV_offTime_, CV_Multiplikator_ * CV_onTime_, CV_onFade_, CV_offFade_, CV_Mode_));
			break;

		case 51:			// Wechselblinker
			decoder.push_back(new Wechselblinker(Port_ + 1, CV_Multiplikator_ * CV_offTime_, CV_Multiplikator_ * CV_onTime_, CV_onFade_, CV_offFade_));
			Port_ = Port_ + 1;
			break;

		case 52: case 53: case 54: case 55:			// diverse Lauflichter
			decoder.push_back(new Lauflicht(Port_ + 1, NumberOfPorts_, CV_Multiplikator_ * CV_offTime_, CV_Multiplikator_ * CV_onTime_, CV_Mode_));
			Port_ = Port_ + NumberOfPorts_ - 1;
			break;

		case 60:			// Hausbeleuchtung
			decoder.push_back(new Hausbeleuchtung(Port_ + 1, NumberOfPorts_, CV_Multiplikator_ * CV_offTime_, CV_Multiplikator_ * CV_onTime_));
			Port_ = Port_ + NumberOfPorts_ - 1;
			break;

		case 61:			// Neonr�hren
			decoder.push_back(new NeonLampen(Port_ + 1, NumberOfPorts_, Chance_));
			Port_ = Port_ + NumberOfPorts_ - 1;
			break;

		case 62:			// Natriumlampen
			decoder.push_back(new NatriumLampen(Port_ + 1, NumberOfPorts_, Chance_, CV_onFade_, CV_offFade_));
			Port_ = Port_ + NumberOfPorts_ - 1;
			break;

		case 80:            // TV
			decoder.push_back(new Fernseher(Port_ + 1));
			break;

		case 81:            // Schweisslicht
			decoder.push_back(new Schweissen(Port_ + 1, CV_Multiplikator_ * CV_offTime_, CV_Multiplikator_ * CV_onTime_));
			Port_ = Port_ + 2;
			break;

		case 82:            // Feuer
			decoder.push_back(new Feuer(Port_ + 1));
			Port_ = Port_ + 2;
			break;

#endif
		} // switch CV_Mode_

#ifdef DEBUG_MSG
		Serial.print(F("  Memory size: ")); Serial.println(freeMemory_ - freeMemory());
		freeMemory_ = freeMemory();
#endif

	} // end for (uint8_t Port_ = 0; Port_ <= 15; Port_++)

	  // PWm Chip freigeben
	digitalWrite(OEPin, LOW);

#ifdef DEBUG_INIT
	Serial.print(F("  Free Memory after Init: "));  Serial.println(freeMemory());
	Serial.println(F("========================================="));
#endif

}

// the setup function runs once when you press reset or power the board
void setup() {
	// Lock the PWM Chip for Init
	pinMode(OEPin, OUTPUT);
	digitalWrite(OEPin, HIGH);

	// Start RASCII
	rascii.begin(57600);

	// Wire must be started!
	Wire.begin();

	// Reset and Init the PCA9685 Chip
	PLED01.resetDevices();
	PLED01.init(PLED01_I2C_Address, PCA9622DR_MODE1_AI2 | PCA9622DR_MODE1_ALLCALL, PCA9622DR_LDR2, PCA9622DR_LDR2, PCA9622DR_LDR2, PCA9622DR_LDR2);

	// Disable all Channels
	for (uint8_t i_ = 0; i_ < PWM_MAXCHANNELS; i_++)
		PLED01.SetChannelOff(i_);

#if !defined(FORCE_RESET_FACTORY_DEFAULT_CV)
	//if eeprom has 0xFF then assume it needs to be programmed
	if (GetCV(100) == 0xFF) {

#ifdef DEBUG_INIT
		Serial.println("CV Defaulting due to blank eeprom");
#endif

		notify_CMD_RESETCONF();

	} else { 
#ifdef DEBUG_INIT
		Serial.println("CV Not Defaulting");
#endif

	}
#else
	Serial.println("CV Defaulting Always On Powerup");
	notify_CMD_RESETCONF();
#endif 

	// Not forcing a reset CV Reset to Factory Defaults
	if (DefaultsCVIndex == 0) {
		initDecoder();
	};

	// Enable the PWM Chip
	digitalWrite(OEPin, LOW);
}

// the loop function runs over and over again until power down or reset
void loop() {
	// Process the RASCII
	rascii.process(myaddr);

	if (DefaultsCVIndex == 0) {

		// Process the Outputs
		for (vector<accessories*>::const_iterator it = decoder.begin(); it != decoder.end(); it++) {
			(*it)->process();
		}
	}
	else {
		digitalWrite(OEPin, HIGH);

		DefaultsCVIndex--; // Decrement first as initially it is the size of the array
		uint8_t cv_ = pgm_read_word(&DefaultCVs[DefaultsCVIndex].CV);
		uint8_t val_ = pgm_read_word(&DefaultCVs[DefaultsCVIndex].Value);

#ifdef DEBUG_MSG
		Serial.print(F("void loop; Set DefaultCV: ")); Serial.print(cv_, DEC); Serial.print(F(" Value: ")); Serial.println(val_, DEC);
#endif 
		SetCV(cv_, val_);
		
		// Is this the last Default CV to set? if so re-init zDecoder
		if (DefaultsCVIndex == 0) {
			destroyDecoder();
			initDecoder();
		}
			

	}


}

extern void notify_CMD_INFO() {
	char s[32];
	sprintf(s, "RASCII_Lightdecoder on addr=%d", myaddr);
	rascii.write(s);
}

extern void notify_CMD_DOUT(byte* cmd) {

	//byte addr = cmd[1];
	//byte port = cmd[2];
	//byte value = cmd[3];

#ifdef  DEBUG_MSG
	Serial.print(F("notify_CMD_DOUT; addr: ")); 
	Serial.print(cmd[1], DEC); 
	Serial.print(F(", port: ")); 
	Serial.print(cmd[2], DEC); 
	Serial.print(F(", value: ")); 
	Serial.println(cmd[3], DEC);
#endif

	// is this our Address?
	if (cmd[1] == myaddr) {

		// Notify all Outputs
		for (vector<accessories*>::const_iterator it = decoder.begin(); it != decoder.end(); it++) {
			(*it)->notifyCommand(cmd[2], cmd[3]);

		}
	}

}

// Send the configuration to the host.
extern void notify_CMD_GETCONF() {
#ifdef  DEBUG_MSG
	Serial.print(F("notify_CMD_GETCONF"));
#endif
	Serial.print(F("notify_CMD_GETCONF"));
	//byte conf[128];
	//memcpy(conf, &boardSetup, sizeof(boardSetup));
	//rascii.write(CMD_GETCONF, conf, sizeof(boardSetup), RSP);

}

extern void notify_CMD_RESETCONF() {

#ifdef  DEBUG_MSG
	Serial.println(F("void notify_CMD_RESETCONF"));
#endif
	// Make FactoryDefaultCVIndex non-zero and equal to num CV's to be reset 
	// to flag to the loop() function that a reset to Factory Defaults needs to be done
	DefaultsCVIndex = sizeof(DefaultCVs) / sizeof(CVPair);
}

extern void notify_CMD_GETCV(byte addr, byte CVHigh, byte CVLow) {
	byte conf[32];
	char s[100];
	int Index_;
	byte Basis_;


	int CV = (CVHigh * 256) + CVLow + 1;

	conf[0] = addr;
	conf[0] = CVHigh;
	conf[1] = CVLow;
	conf[2] = GetCV(CV);		// Value

	rascii.write(CMD_GETCV, conf, 3, RSP);

#ifdef  DEBUG_MSG
	Serial.print(F("notify_CMD_GETCV; addr: "));
	Serial.print(addr, DEC);
	Serial.print(F(", CV: "));
	Serial.print(CV, DEC);	Serial.print(F(", value: "));
	Serial.println(conf[2], DEC);
#endif


}

extern void notify_CMD_SETCV(byte addr, byte CVHigh, byte CVLow, byte value) {
	char s[100];

	int CV = (CVHigh * 256) + CVLow + 1;

	if ((CV == CV_Reset) && (value == CV_Reset)) {
		notify_CMD_RESETCONF();
		sprintf(s, "reset %d = %d", CV, value);
		rascii.write(s);
	}
	else {
		SetCV(CV, value);
		sprintf(s, "set cv %d to %d", CV, value);
		rascii.write(s);
	}

#ifdef  DEBUG_MSG
	Serial.print(F("void notify_CMD_SETCV; addr: "));
	Serial.print(addr, DEC);
	Serial.print(F(", CV: "));
	Serial.print(CV, DEC);
	Serial.print(F(", value: "));
	Serial.println(value, DEC);
#endif


}

void notify_CVChange(uint16_t CV_, uint8_t value_) {
#ifdef DEBUG_MSG
	Serial.print(F("void notifyCVChange: CV: "));
	Serial.print(CV_, DEC);
	Serial.print(F(" Value: "));
	Serial.println(value_, DEC);
#endif  

	// Es wurde ein CV ge�ndert. 
	destroyDecoder();

	// Set default CV's when Mode was changed
	if ((CV_ >= 100) && (CV_ < 120)) {
		switch (value_) {
		case 40:
			SetCV(CV_ + 20, 0);			// onTime
			SetCV(CV_ + 40, 0);			// offTime
			SetCV(CV_ + 60, 0);			// Multiplikator
			SetCV(CV_ + 80, 0);			// onFade
			SetCV(CV_ + 100, 0);		// offFade
			SetCV(CV_ + 120, 0);		// Brightness Day
			SetCV(CV_ + 140, 0);		// Brightness Night
			break;

			// Blinker, Wechselblinker
		case 50: case 51:
			SetCV(CV_ + 20, 100);		// onTime
			SetCV(CV_ + 40, 100);		// offTime
			SetCV(CV_ + 60, 10);		// Multiplikator
			SetCV(CV_ + 80, 50);		// onFade
			SetCV(CV_ + 100, 50);		// offFade
			SetCV(CV_ + 120, 255);		// Brightness Day
			SetCV(CV_ + 140, 100);		// Brightness Night
			break;

			// Lauflichter
		case 52: case 53: case 54:
			SetCV(CV_ + 1, 6);			// Mode 2
			SetCV(CV_ + 20, 100);			// onTime
			SetCV(CV_ + 40, 100);		// offTime
			SetCV(CV_ + 60, 10);		// Multiplikator
			SetCV(CV_ + 80, 100);		// onFade
			SetCV(CV_ + 100, 100);		// offFade
			SetCV(CV_ + 120, 255);		// Brightness Day
			SetCV(CV_ + 140, 100);		// Brightness Night
			break;

			// Hausbelecuhtung
		case 60:
			SetCV(CV_ + 1, 6);			// Mode 2
			SetCV(CV_ + 20, 5);			// onTime
			SetCV(CV_ + 40, 15);		// offTime
			SetCV(CV_ + 60, 1);			// Multiplikator
			SetCV(CV_ + 80, 0);			// onFade
			SetCV(CV_ + 100, 0);		// offFade
			SetCV(CV_ + 120, 255);		// Brightness Day
			SetCV(CV_ + 140, 100);		// Brightness Night
			break;

			// Neonlampen
		case 61:
			SetCV(CV_ + 1, 6);			// Mode 2
			SetCV(CV_ + 20, 0);			// onTime
			SetCV(CV_ + 40, 0);			// offTime
			SetCV(CV_ + 60, 0);			// Multiplikator
			SetCV(CV_ + 80, 0);			// onFade
			SetCV(CV_ + 100, 0);		// offFade
			SetCV(CV_ + 120, 255);		// Brightness Day
			SetCV(CV_ + 140, 100);		// Brightness Night
			break;

			// Natriumlampen
		case 62:
			SetCV(CV_ + 1, 6);			// Mode 2
			SetCV(CV_ + 20, 1);			// onTime
			SetCV(CV_ + 40, 1);			// offTime
			SetCV(CV_ + 60, 0);			// Multiplikator
			SetCV(CV_ + 80, 0);			// onFade
			SetCV(CV_ + 100, 0);		// offFade
			SetCV(CV_ + 120, 255);		// Brightness Day
			SetCV(CV_ + 140, 100);		// Brightness Night
			break;

			// TV
		case 80:
			SetCV(CV_ + 20, 0);			// onTime
			SetCV(CV_ + 40, 0);			// offTime
			SetCV(CV_ + 60, 0);			// Multiplikator
			SetCV(CV_ + 80, 0);			// onFade
			SetCV(CV_ + 100, 0);		// offFade
			SetCV(CV_ + 120, 255);		// Brightness Day
			SetCV(CV_ + 140, 100);		// Brightness Night
			break;

			// Sweisslicht
		case 81:
			SetCV(CV_ + 20, 5);			// onTime
			SetCV(CV_ + 40, 15);			// offTime
			SetCV(CV_ + 60, 0);			// Multiplikator
			SetCV(CV_ + 80, 0);			// onFade
			SetCV(CV_ + 100, 0);		// offFade
			SetCV(CV_ + 120, 255);		// Brightness Day
			SetCV(CV_ + 140, 100);		// Brightness Night
			break;

			// Feuer
		case 82:
			SetCV(CV_ + 20, 0);			// onTime
			SetCV(CV_ + 40, 0);			// offTime
			SetCV(CV_ + 60, 0);			// Multiplikator
			SetCV(CV_ + 80, 0);			// onFade
			SetCV(CV_ + 100, 0);		// offFade
			SetCV(CV_ + 120, 255);		// Brightness Day
			SetCV(CV_ + 140, 100);		// Brightness Night
			break;


		}

	}

	// Der Decoder muss neu initialisiert werden
	initDecoder();
}
