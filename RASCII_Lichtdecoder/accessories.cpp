/*
Copyright (c) 2018 andy      <don_funk@bluewin.ch>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created by Andreas Zogg, September 26, 2018.

accessories.cpp		Version 1.0.0
*/


#include "accessories.h"

// Class accessories
accessories::accessories(byte BaseChannel_) :
	m_BaseChannel(BaseChannel_) {
};

accessories::~accessories() {
};

void accessories::process() {
}

void accessories::notifyCommand(uint8_t Port, uint8_t Value) {
};


