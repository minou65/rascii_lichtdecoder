Copyright (c) 2018 andy      <don_funk@bluewin.ch>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Ein Decoder basierend auf dem Rocrail RASCII Protokoll

Alle Ausgänge sind PWM und können individuell angesteuert werden

Folgende Funktionen können mittels CV ausgewählt werden:
- Ausgang
- Blinker
- Wechselblinker
- Lauflicht (Lampe für Lampe wird eingeschaltet und ausgeschaltet)
- Lauflicht (Lampe für Lampe wird eingeschaltet. Wenn alle eingeschaltet sind, werden alle gelöscht.)
- Lauflicht (Lampe für Lampe wird eingeschaltet. Sind alle eingeschaltet, wird mit der ersten Lampe begonnen zu löschen.)
- Hausbeleuchtung
- Neonröhren
- Natriumdampflampen
- Fernseher
- Schweisslicht
- Feuer

Für mehr Informationen
https://wiki.rocrail.net/doku.php?id=wiki:user:minou65:rasciilightdecoder-de

